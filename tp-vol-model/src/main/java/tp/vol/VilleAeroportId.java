package tp.vol;

import java.io.Serializable;

public class VilleAeroportId implements Serializable{
	private String code_aeroport;
	private String ville_id;
	
	public VilleAeroportId() {
		super();
	}

	public String getCode_aeroport() {
		return code_aeroport;
	}

	public void setCode_aeroport(String code_aeroport) {
		this.code_aeroport = code_aeroport;
	}

	public String getVille_id() {
		return ville_id;
	}

	public void setVille_id(String ville_id) {
		this.ville_id = ville_id;
	}

}
