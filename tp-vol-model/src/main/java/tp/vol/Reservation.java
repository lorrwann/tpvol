package tp.vol;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Reservation {
	@Id
	@GeneratedValue
	private Long id;
	@Column(length = 100)
	private String numero;
	@Column
	private boolean statut;
	@Column
	private Float tarif;
	@Column
	private Float tauxTVA;
	@Temporal(TemporalType.DATE)
	private Date dateReservation;

	@ManyToOne
	@JoinColumn(name = "id_client")
	
	private Client client;
	@ManyToOne
	@JoinColumn(name = "id_voyage")
	
	private Voyage voyage;
	@ManyToOne
	@JoinColumn(name = "id_passager")
	
	private Passager passager;

	public Reservation() {
		super();
	}

	public Reservation(Long id, String numero, boolean statut, Float tarif, Float tauxTVA, Date dateReservation) {
		super();
		this.id = id;
		this.numero = numero;
		this.statut = statut;
		this.tarif = tarif;
		this.tauxTVA = tauxTVA;
		this.dateReservation = dateReservation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public boolean isStatut() {
		return statut;
	}

	public void setStatut(boolean statut) {
		this.statut = statut;
	}

	public Float getTarif() {
		return tarif;
	}

	public void setTarif(Float tarif) {
		this.tarif = tarif;
	}

	public Float getTauxTVA() {
		return tauxTVA;
	}

	public void setTauxTVA(Float tauxTVA) {
		this.tauxTVA = tauxTVA;
	}

	public Date getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}

	public Passager getPassager() {
		return passager;
	}

	public void setPassager(Passager passager) {
		this.passager = passager;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Voyage getVoyage() {
		return voyage;
	}

	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}

}
