package tp.vol;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="aeroport")	
public class Aeroport {
	
	@Id
	@Column(name="code", length = 10)
	private String code;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
	name = ("AeroportVilleId"),
	joinColumns = @JoinColumn (name = "code_aeroport"),
	inverseJoinColumns = @JoinColumn (name = "ville_id")
	)
	private List<Ville> villes = new ArrayList<Ville>();

	public Aeroport() {
		super();
	}

	public Aeroport(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

}
