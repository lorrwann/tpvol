package tp.vol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="voyage_vol")
public class VoyageVol {
	@Id
	@GeneratedValue
	private Long id;
	 @Column
	private int ordre;
	@ManyToOne
	@JoinColumn(name = "id_voyage")
	private Voyage voyage;
	 
	@ManyToOne
	@JoinColumn(name="id_vol")
	private Vol vol;

	public VoyageVol() {
		super();
	}

	public VoyageVol(Long id, int ordre) {
		super();
		this.id = id;
		this.ordre = ordre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}

	public Voyage getVoyage() {
		return voyage;
	}

	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}

	public Vol getVol() {
		return vol;
	}

	public void setVol(Vol vol) {
		this.vol = vol;
	}

}
