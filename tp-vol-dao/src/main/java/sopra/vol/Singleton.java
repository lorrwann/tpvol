package sopra.vol;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sopra.vol.dao.IAeroportDao;
import sopra.vol.dao.IClientDao;
import sopra.vol.dao.ICompagnieDao;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationDao;
import sopra.vol.dao.IVilleDao;
import sopra.vol.dao.IVolDao;
import sopra.vol.dao.IVoyageDao;
import sopra.vol.dao.IVoyageVolDao;
import sopra.vol.dao.jpa.AeroportDaoJpa;
import sopra.vol.dao.jpa.ClientDaoJpa;
import sopra.vol.dao.jpa.CompagnieDaoJpa;
import sopra.vol.dao.jpa.PassagerDaoJpa;
import sopra.vol.dao.jpa.ReservationDaoJpa;
import sopra.vol.dao.jpa.VilleDaoJpa;
import sopra.vol.dao.jpa.VolDaoJpa;
import sopra.vol.dao.jpa.VoyageDaoJpa;
import sopra.vol.dao.jpa.VoyageVolDaoJpa;

public class Singleton {
	private static Singleton instance = null;
	private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-vol");
	private final ICompagnieDao compagnieDao=new  CompagnieDaoJpa();
	private final IVolDao volDao=new  VolDaoJpa();
	private final IVoyageVolDao voyageVol= new VoyageVolDaoJpa();
	private final IClientDao clientDao=new ClientDaoJpa();
	private final IPassagerDao passagerDao= new PassagerDaoJpa();
	private final IReservationDao reservationDao= new ReservationDaoJpa();
	private final IVoyageDao voyageDao=new VoyageDaoJpa();
	private final IAeroportDao aeroportDao=new AeroportDaoJpa();
	private final IVilleDao villeDao=new VilleDaoJpa();
	
	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}

		return instance;
	}
	public EntityManagerFactory getEmf() {
		return emf;
	}

	public  ICompagnieDao getCompagnieDao() {
		return compagnieDao;
	}
	public  IVolDao getVolDao() {
		return volDao;
	}
	public IVoyageVolDao getVoyageVol() {
		return voyageVol;
	}
	public IClientDao getclient() {
		return clientDao;
	}
	public IPassagerDao getPassager() {
		return passagerDao;
	}
	public IReservationDao getReservation() {
		return reservationDao;
	}
	public IVoyageDao getVoyage() {
		return voyageDao;
	}
	public IAeroportDao getAeroport() {
		return aeroportDao;
	}
	public IVilleDao getVille() {
		return villeDao;
	}
	}
