package sopra.vol.dao;

import tp.vol.Reservation;

public interface IReservationDao extends IDao<Reservation, Long> {
}
