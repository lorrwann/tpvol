package sopra.vol.dao;

import tp.vol.Vol;

public interface IVolDao extends IDao<Vol, Long> {
}
