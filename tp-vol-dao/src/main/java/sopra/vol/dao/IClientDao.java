package sopra.vol.dao;

import java.util.List;

import tp.vol.Client;
import tp.vol.ClientParticulier;
import tp.vol.ClientPro;

public interface IClientDao extends IDao<Client, Long>{
	List<ClientParticulier> findAllClientParticulier();
	List<ClientPro> findAllClientPro();
}
