package sopra.vol.dao;

import tp.vol.VoyageVol;

public interface IVoyageVolDao extends IDao<VoyageVol, Long> {
}
