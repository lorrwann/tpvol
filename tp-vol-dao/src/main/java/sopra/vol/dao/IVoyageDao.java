package sopra.vol.dao;

import tp.vol.Voyage;

public interface IVoyageDao extends IDao<Voyage, Long> {
}
