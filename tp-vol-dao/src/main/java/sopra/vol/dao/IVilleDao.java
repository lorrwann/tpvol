package sopra.vol.dao;

import tp.vol.Ville;

public interface IVilleDao extends IDao<Ville, Long> {
}
