package sopra.vol.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import sopra.vol.Singleton;
import sopra.vol.dao.IClientDao;
import tp.vol.Client;
import tp.vol.ClientParticulier;
import tp.vol.ClientPro;

public class ClientDaoJpa implements IClientDao{

	@Override
	public List<Client> findAll() {
		List<Client> listeClients = new ArrayList<Client>();

		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();
			
			TypedQuery<Client> query = em.createQuery("select c from Client c", Client.class);
			
			listeClients = query.getResultList();
			
			tx.commit();
			
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return listeClients;
	}

	@Override
	public Client findById(Long id) {
		Client obj = null;
		
		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
		em = Singleton.getInstance().getEmf().createEntityManager();
		tx = em.getTransaction();
		tx.begin();

		obj = em.find(Client.class, id);
		tx.commit();
	} catch (Exception e) {
		if (tx != null && tx.isActive()) {
			tx.rollback();
		}
		e.printStackTrace();
	} finally {
		if (em != null) {
			em.close();
		}
	}
		
		return obj;
	}

	@Override
	public Client save(Client obj) {
		
		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
		em = Singleton.getInstance().getEmf().createEntityManager();
		tx = em.getTransaction();
		tx.begin();

		obj = em.merge(obj);
		
		tx.commit();
	} catch (Exception e) {
		if (tx != null && tx.isActive()) {
			tx.rollback();
		}
		e.printStackTrace();
	} finally {
		if (em != null) {
			em.close();
		}
	}
		return obj;
	}

	@Override
	public void delete(Client obj) {
		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
		em = Singleton.getInstance().getEmf().createEntityManager();
		tx = em.getTransaction();
		tx.begin();

		em.remove(em.merge(obj));
		
		tx.commit();
	} catch (Exception e) {
		if (tx != null && tx.isActive()) {
			tx.rollback();
		}
		e.printStackTrace();
	} finally {
		if (em != null) {
			em.close();
		}
	}
		
	}

	@Override
	public List<ClientParticulier> findAllClientParticulier() {
		List<ClientParticulier> listeClientsParticuliers = new ArrayList<ClientParticulier>();

		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();
			
			TypedQuery<ClientParticulier> query = em.createQuery("select cpa from ClientParticulier cpa", ClientParticulier.class);
			
			listeClientsParticuliers = query.getResultList();
			
			tx.commit();
			
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return (List<ClientParticulier>) listeClientsParticuliers;
	}

	@Override
	public List<ClientPro> findAllClientPro() {
		List<ClientPro> listeClientsPros = new ArrayList<ClientPro>();

		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
			em = Singleton.getInstance().getEmf().createEntityManager();
			tx = em.getTransaction();
			tx.begin();
			
			TypedQuery<ClientPro> query = em.createQuery("select cpo from ClientPro cpo", ClientPro.class);
			
			listeClientsPros = query.getResultList();
			
			tx.commit();
			
		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return (List<ClientPro>) listeClientsPros;
	}

}
