package sopra.formation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import sopra.vol.Singleton;
import tp.vol.Aeroport;
import tp.vol.Ville;

public class DonneesAeroportVille {

	public static void main(String[] args) {
		EntityManagerFactory emf = Singleton.getInstance().getEmf();

		Ville bordeaux = null;
		Ville gradignan = null;
		Ville toulouse = null;
		Ville marseille = null;
		Ville paris = null;
		
		Aeroport bod = null;
		Aeroport tls = null;
		Aeroport mrs = null;
		Aeroport cdg = null;
		Aeroport ory = null;
		
		EntityManager em = null;
		EntityTransaction tx = null;
		
		try {
			em = emf.createEntityManager();
			tx = em.getTransaction();
			tx.begin();
			
			bordeaux = new Ville();
			bordeaux.setNom("Bordeaux");
			
			gradignan = new Ville();
			gradignan.setNom("Gradignan");
			
			toulouse = new Ville();
			toulouse.setNom("Toulouse");
			
			marseille = new Ville();
			marseille.setNom("Marseille");
			
			paris = new Ville();
			paris.setNom("Paris");
			
			List<Ville> villes = new ArrayList<Ville>();
			villes.add(bordeaux);
			villes.add(gradignan);
			villes.add(toulouse);
			villes.add(marseille);
			villes.add(paris);
			
			List<Ville> villesbod = new ArrayList<Ville>();
			villesbod.add(bordeaux);
			villesbod.add(gradignan);
			
			List<Ville> villestls= new ArrayList<Ville>();
			villestls.add(toulouse);
			
			List<Ville> villesmrs= new ArrayList<Ville>();
			villesmrs.add(marseille);
			
			List<Ville> villesparis = new ArrayList<Ville>();
			villesparis.add(paris);
			
			
			bod = new Aeroport();
			bod.setCode("BOD");
			bod.setVilles(villesbod);
			
			tls = new Aeroport();
			tls.setCode("TLS");
			tls.setVilles(villestls);
			
			mrs = new Aeroport();
			mrs.setCode("MRS");
			mrs.setVilles(villesmrs);
			
			cdg = new Aeroport();
			cdg.setCode("CDG");
			
			ory = new Aeroport();
			ory.setCode("ORY");
			
			List<Aeroport> aeroports = new ArrayList<Aeroport>();
			aeroports.add(bod);
			aeroports.add(tls);
			aeroports.add(mrs);
			aeroports.add(cdg);
			aeroports.add(ory);
			
			List<Aeroport> aeroportsbod = new ArrayList<Aeroport>();
			aeroportsbod.add(bod);
			
			List<Aeroport> aeroportstls = new ArrayList<Aeroport>();
			aeroportstls.add(tls);
			
			List<Aeroport> aeroportsmrs = new ArrayList<Aeroport>();
			aeroportsmrs.add(mrs);
			
			List<Aeroport> aeroportsparis = new ArrayList<Aeroport>();
			aeroportsparis.add(cdg);
			aeroportsparis.add(ory);
			
			em.persist(bordeaux);
			em.persist(gradignan);
			em.persist(toulouse);
			em.persist(marseille);
			em.persist(paris);
			
			em.persist(bod);
			em.persist(tls);
			em.persist(mrs);
			em.persist(cdg);
			em.persist(ory);
			
			
			tx.commit();
			
     } catch (Exception e) {
		if (tx != null && tx.isActive()) {
			tx.rollback();
		}
		e.printStackTrace();
	 } finally {
		if (em != null) {
			em.close();
		}
	}
		emf.close();	
	}			
			
}	


