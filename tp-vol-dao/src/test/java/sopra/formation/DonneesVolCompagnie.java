package sopra.formation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import sopra.vol.Singleton;
import tp.vol.Adresse;
import tp.vol.Aeroport;
import tp.vol.Civilite;
import tp.vol.Client;
import tp.vol.ClientParticulier;
import tp.vol.ClientPro;
import tp.vol.Compagnie;
import tp.vol.MoyenPaiement;
import tp.vol.Passager;
import tp.vol.Reservation;
import tp.vol.TypePieceIdentite;
import tp.vol.Ville;
import tp.vol.Vol;
import tp.vol.Voyage;

public class DonneesVolCompagnie {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		EntityManagerFactory emf = Singleton.getInstance().getEmf();
		
		Ville bordeaux = null;
		Ville gradignan = null;
		Ville toulouse = null;
		Ville marseille = null;
		Ville paris = null;
		
		Aeroport bod = null;
		Aeroport tls = null;
		Aeroport mrs = null;
		Aeroport cdg = null;
		Aeroport ory = null;
		
		Compagnie airFrance = null;
		Compagnie klm = null;
		
		Vol vol1 = null;
		Vol vol2 = null;
		
		Voyage voyageglobal = null;
		
		Reservation resa1 = null;
		Reservation resa2 = null;
		
		ClientPro sopra = null;
		ClientParticulier client1 = null;
		
		Passager passager1 = null;
		
		Adresse adresse1 = null;
		Adresse adresse2 = null;
		
		EntityManager em = null;
		EntityTransaction tx = null;
		
	try {
		em = emf.createEntityManager();
		tx = em.getTransaction();
		tx.begin();
		
		bordeaux = new Ville();
		bordeaux.setNom("Bordeaux");
		
		gradignan = new Ville();
		gradignan.setNom("Gradignan");
		
		toulouse = new Ville();
		toulouse.setNom("Toulouse");
		
		marseille = new Ville();
		marseille.setNom("Marseille");
		
		paris = new Ville();
		paris.setNom("Paris");
		
		List<Ville> villes = new ArrayList<Ville>();
		villes.add(bordeaux);
		villes.add(gradignan);
		villes.add(toulouse);
		villes.add(marseille);
		villes.add(paris);
		
		List<Ville> villesbod = new ArrayList<Ville>();
		villesbod.add(bordeaux);
		villesbod.add(gradignan);
		
		List<Ville> villestls= new ArrayList<Ville>();
		villestls.add(toulouse);
		
		List<Ville> villesmrs= new ArrayList<Ville>();
		villesmrs.add(marseille);
		
		List<Ville> villesparis = new ArrayList<Ville>();
		villesparis.add(paris);
		
		
		bod = new Aeroport();
		bod.setCode("BOD");
		bod.setVilles(villesbod);
		
		tls = new Aeroport();
		tls.setCode("TLS");
		tls.setVilles(villestls);
		
		mrs = new Aeroport();
		mrs.setCode("MRS");
		mrs.setVilles(villesmrs);
		
		cdg = new Aeroport();
		cdg.setCode("CDG");
		
		ory = new Aeroport();
		ory.setCode("ORY");
		
		List<Aeroport> aeroports = new ArrayList<Aeroport>();
		aeroports.add(bod);
		aeroports.add(tls);
		aeroports.add(mrs);
		aeroports.add(cdg);
		aeroports.add(ory);
		
		List<Aeroport> aeroportsbod = new ArrayList<Aeroport>();
		aeroportsbod.add(bod);
		
		List<Aeroport> aeroportstls = new ArrayList<Aeroport>();
		aeroportstls.add(tls);
		
		List<Aeroport> aeroportsmrs = new ArrayList<Aeroport>();
		aeroportsmrs.add(mrs);
		
		List<Aeroport> aeroportsparis = new ArrayList<Aeroport>();
		aeroportsparis.add(cdg);
		aeroportsparis.add(ory);
		
		
		vol1 = new Vol();
		vol1.setCompagnie(klm);
		vol1.setDepart(tls);
		vol1.setDateArrivee(sdf.parse("26/07/2019"));
		vol1.setArrivee(bod);
		vol1.setDateArrivee(sdf.parse("27/07/2019"));
		vol1.setNbPlaces(70);
		vol1.setOuvert(true);
		
		
		vol2 = new Vol();
		vol2.setCompagnie(airFrance);
		vol2.setDepart(bod);
		vol2.setDateDepart(sdf.parse("28/07/2019"));
		vol2.setArrivee(cdg);
		vol2.setDateArrivee(sdf.parse("29/07/2019"));
		vol2.setNbPlaces(100);
		vol2.setOuvert(true);
		
		voyageglobal = new Voyage();
		
		adresse1 = new Adresse();
		adresse1.setCodePostal("33123");
		adresse1.setPays("France");
		adresse1.setVille("Ville du monde");
		adresse1.setVoie("6 rue de la montagne");
		
		adresse2 = new Adresse();
		adresse2.setCodePostal("33000");
		adresse2.setPays("France");
		adresse2.setVille("Ville de Bordeaux");
		adresse2.setVoie("6 rue de la plaine");
		
		resa1 = new Reservation();
		resa1.setClient(client1);
		resa1.setDateReservation(sdf.parse("05/05/2005"));
		resa1.setPassager(passager1);
		resa1.setStatut(true);
		resa1.setTarif(250f);
		
		resa2 = new Reservation();
		resa2.setClient(client1);
		resa2.setDateReservation(sdf.parse("05/05/2005"));
		resa2.setPassager(passager1);
		resa2.setStatut(true);
		resa2.setTarif(500f);
		
		List<Reservation> resaRemy = new ArrayList<Reservation>();
		resaRemy.add(resa1);
		resaRemy.add(resa2);
		
		List<Vol> volKlm = new ArrayList<Vol>();
		volKlm.add(vol1);
		
		List<Vol> volAirFrance = new ArrayList<Vol>();
		volAirFrance.add(vol2);
		
		airFrance = new Compagnie();
		airFrance.setNomCompagnie("Air France");
		airFrance.setVols(volAirFrance);
		
		klm = new Compagnie();
		klm.setNomCompagnie("KLM");
		klm.setVols(volKlm);
		
		sopra = new ClientPro();
		sopra.setMail("sopra@lalala.com");
		sopra.setNomEntreprise("Sopra Steria");
		sopra.setNumeroSiret("azj545e4a5e4");
		sopra.setNumTVA("keja464534");
		sopra.setTelephone("0687459216");
		sopra.setPrincipale(adresse1);
		sopra.setFacturation(adresse1);
		
		client1 = new ClientParticulier();
		client1.setCivilite(Civilite.M);
		client1.setMail("remy330@msn.com");
		client1.setMoyenPaiement(MoyenPaiement.ESPECE);
		client1.setPrenom("remy");
		client1.setNom("renom");
		client1.setPrincipale(adresse1);
		client1.setReservations(resaRemy);
		
		passager1 = new Passager();
		passager1.setCivilite(Civilite.M);
		passager1.setNom("NomDuPassager");
		passager1.setPrenom("Prenom Du Passager");
		passager1.setNationalite("Française");
		passager1.setMail("pass@msn.com");
		passager1.setReservations(resaRemy);
		passager1.setTypePI(TypePieceIdentite.CARTE_IDENTITE);
		
		
		
		em.persist(bordeaux);
		em.persist(gradignan);
		em.persist(toulouse);
		em.persist(marseille);
		em.persist(paris);
		
		em.persist(bod);
		em.persist(tls);
		em.persist(mrs);
		em.persist(cdg);
		em.persist(ory);
		
		em.persist(vol1);
		em.persist(vol2);
		em.persist(sopra);
		em.persist(passager1);
		em.persist(client1);
		em.persist(klm);
		em.persist(airFrance);
		em.persist(resa1);
		em.persist(resa2);		
		
		tx.commit();
	
    } catch (Exception e) {
		if (tx != null && tx.isActive()) {
			tx.rollback();
		}
		e.printStackTrace();
	 } finally {
		if (em != null) {
			em.close();
		}
	}
		emf.close();	
	}	

}
